package fi.jamk.k8039.basicuicontrolls2;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // AutoCompleteTextView
        AutoCompleteTextView actv = (AutoCompleteTextView) findViewById(R.id.loginText); // add strings to control
        ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, new String[]{
                "Pasi","Juha","Kari","Jouni","Esa","Hannu"
        });
        actv.setAdapter(aa);
    }

    public void loginButtonClicked(View view) {
        EditText login = (EditText) findViewById(R.id.loginText);
        String text = (String) login.getText().toString();

        EditText password = (EditText) findViewById(R.id.passwordText);
        String textpw = (String) password.getText().toString();
// toast message to screen
        Toast.makeText(getApplicationContext(), text+" "+textpw,
                Toast.LENGTH_SHORT).show();
    }
}
