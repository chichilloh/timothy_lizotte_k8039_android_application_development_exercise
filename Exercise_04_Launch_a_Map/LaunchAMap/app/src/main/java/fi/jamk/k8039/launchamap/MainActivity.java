package fi.jamk.k8039.launchamap;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private final int SHOW_NEWACTIVITY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showMap(View view) {
// get lat and lng values
        EditText editText1 = (EditText) findViewById(R.id.lat);
        EditText editText2 = (EditText) findViewById(R.id.lng);
        String numberOne = editText1.getText().toString();
        String numberTwo = editText2.getText().toString();
        double lat = Double.parseDouble(numberOne);
        double lng = Double.parseDouble(numberTwo);
// show map
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("geo: "+lat+","+lng));
        Toast.makeText(this, lat+","+lng, Toast.LENGTH_LONG).show();
        startActivity(intent);
    }
}
