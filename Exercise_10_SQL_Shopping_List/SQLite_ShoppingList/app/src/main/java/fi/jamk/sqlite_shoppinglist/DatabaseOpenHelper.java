package fi.jamk.sqlite_shoppinglist;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseOpenHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "PTM_database";
    private final String DATABASE_TABLE = "shoppingList";
    private final String PRODUCT = "itemName";
    private final String QANTITY = "itemAmount";
    private final String PRICE = "itemPrice";

    public DatabaseOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+DATABASE_TABLE+" (_id INTEGER PRIMARY KEY AUTOINCREMENT, "+PRODUCT+" TEXT, "+QANTITY+" REAL, " + PRICE+" FLOAT);");
        ContentValues values = new ContentValues();
        values.put(PRODUCT, "Milk");
        values.put(QANTITY, 2);
        values.put(PRICE,0.89);
        db.insert(DATABASE_TABLE, null, values);
        values.put(PRODUCT, "Eggs");
        values.put(QANTITY, 1);
        values.put(PRICE, 1.69);
        db.insert(DATABASE_TABLE, null, values);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+DATABASE_TABLE);
        onCreate(db);
    }

}
