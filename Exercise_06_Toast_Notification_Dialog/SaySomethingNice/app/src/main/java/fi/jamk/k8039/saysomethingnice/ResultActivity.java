package fi.jamk.k8039.saysomethingnice;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class ResultActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void backButtonClicked(View view) {
        finish();
    }

}