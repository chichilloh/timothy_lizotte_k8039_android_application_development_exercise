package fi.jamk.k8039.saysomethingnice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class DialogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
    }

    public void createNiceDialog(View view){
        NiceDialogFragment eDialog = new NiceDialogFragment();
        eDialog.show(getFragmentManager(), "exit");
    }

    public void backButtonClicked(View view){
        finish();
    }

}
