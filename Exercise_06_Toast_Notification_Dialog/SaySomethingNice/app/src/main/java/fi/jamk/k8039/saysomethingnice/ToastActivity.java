package fi.jamk.k8039.saysomethingnice;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ToastActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toast);
    }

    public void toastSomethingNice(View view) {
        Toast.makeText(getBaseContext(), R.string.nice1, Toast.LENGTH_SHORT).show();
    }

    public void backButtonClicked(View view){
        finish();
    }
}