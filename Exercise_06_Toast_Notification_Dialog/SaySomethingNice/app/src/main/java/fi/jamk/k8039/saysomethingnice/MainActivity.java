package fi.jamk.k8039.saysomethingnice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private final int SHOW_NEWACTIVITY=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_toast:
                //Toast.makeText(getBaseContext(), "Toast", Toast.LENGTH_SHORT).show();
                startToastActivity();
                return true;
            case R.id.action_notification:
                //Toast.makeText(getBaseContext(), "Notification", Toast.LENGTH_SHORT).show();
                startNotificationActivity();
                return true;
            case R.id.action_dialog:
                //Toast.makeText(getBaseContext(), "Dialog", Toast.LENGTH_SHORT).show();
                startDialogActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void startToastActivity(){
        //start a new activity
        Intent intent = new Intent(this,ToastActivity.class);
        startActivity(intent);
    }

    public void startNotificationActivity(){
        Intent intent = new Intent(this,NotificationActivity.class);
        startActivity(intent);
    }

    public void startDialogActivity(){
        Intent intent = new Intent(this,DialogActivity.class);
        startActivity(intent);
    }



}
